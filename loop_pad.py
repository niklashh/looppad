import sys, time, mido


if len(sys.argv) > 1:
    portname = sys.argv[1]
else:
    print('Specify a port')
    sys.exit(1)

def filter_notes(port):
    for message in port:
        if message.type in ('note_on', 'note_off'):
            yield message

class Pad:
    def __init__(self, outport, note, epoch, tempo, pattern, align):
        self.outport = outport
        self.note = note
        self.epoch = epoch
        self.tempo = tempo
        self.pattern = pattern
        self.align = align
        self.velocity = 100

        self.on = False
        self.noteOn = False
        self.aligned = False
        self.last_beat = 0
        self.patternHead = len(self.pattern)-1
        self.noteOnTime = 0.05 * 10**9
        self.last_t = 0

    def sendMessage(self, message):
        if message.note == self.note:
            if message.type == 'note_on':
                self.on = True
            else:
                self.on = False
                self.patternHead = len(self.pattern)-1
                self.last_beat = 0
                self.aligned = False

    def update(self):
        t = time.time_ns()
        if self.on:
            if not self.aligned and \
            (self.last_t-self.epoch)//(4*60/self.tempo*10**9*self.align) \
            < (t-self.epoch)//(4*60/self.tempo*10**9*self.align):
                print('Aligned!')
                self.aligned = True
            if self.aligned and t - self.last_beat > \
            self.pattern[self.patternHead] * 4 * 60/self.tempo * 10**9:
                # TODO this is not accurate
                # should add the delta every time
                self.last_beat = t
                self.noteOn = True
                self.patternHead += 1
                self.patternHead %= len(self.pattern)
                msg = mido.Message('note_on',
                        note=self.note,
                        velocity=self.velocity)
                outport.send(msg)
                print(f'{self.note} @ {self.patternHead}')
        if self.noteOn and t - self.last_beat > self.noteOnTime:
            msg = mido.Message('note_off', note=self.note)
            outport.send(msg)
            self.noteOn = False
        self.last_t = t

tempo = 60
epoch = time.time_ns()
try:
    with mido.open_input(portname) as inport:
        with mido.open_output(autoreset=True) as outport:
            pad1 = Pad(outport, 36, epoch, tempo, [1/2], 1/2)
            pad2 = Pad(outport, 38, epoch, tempo, [1/2], 1/4)
            pad3 = Pad(outport, 46, epoch, tempo, [1/1], 7/8)
            pad4 = Pad(outport, 42, epoch, tempo, [1/8], 1/8)
            pads = [pad1, pad2, pad4, pad3]
            print('Input Port: {}'.format(inport))
            print('Output Port: {}'.format(outport))
            while True:
                for message in filter_notes(inport.iter_pending()):
                    print('Received {}'.format(message))
                    for p in pads: p.sendMessage(message)
                    sys.stdout.flush()
                for p in pads: p.update()
                time.sleep(0.01)

except KeyboardInterrupt:
    print(' exiting...')
